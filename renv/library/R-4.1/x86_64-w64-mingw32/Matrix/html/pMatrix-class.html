<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Permutation matrices</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for pMatrix-class {Matrix}"><tr><td>pMatrix-class {Matrix}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Permutation matrices</h2>

<h3>Description</h3>

<p>The <code>pMatrix</code> class is the class of <em>permutation</em> matrices,
stored as 1-based integer permutation vectors.  A permutation
matrix is a square matrix whose rows <em>and</em> columns are all
standard unit vectors.  It follows that permutation matrices are
a special case of <em>index</em> matrices (hence <code>pMatrix</code>
is defined as a direct subclass of <code><a href="../../Matrix/help/indMatrix-class.html">indMatrix</a></code>).
</p>
<p>Multiplying a matrix on the left by a permutation matrix is
equivalent to permuting its rows.  Analogously, multiplying a
matrix on the right by a permutation matrix is equivalent to
permuting its columns.  Indeed, such products are implemented in
<span class="pkg">Matrix</span> as indexing operations; see &lsquo;Details&rsquo; below.
</p>


<h3>Details</h3>

<p>By definition, a permutation matrix is both a row index matrix
and a column index matrix.  However, the <code>perm</code> slot of
a <code>pMatrix</code> cannot be used interchangeably as a row index
vector and column index vector.  If <code>margin=1</code>, then
<code>perm</code> is a row index vector, and the corresponding column
index vector can be computed as <code><a href="../../Matrix/help/invPerm.html">invPerm</a>(perm)</code>, i.e.,
by inverting the permutation.  Analogously, if <code>margin=2</code>,
then <code>perm</code> and <code>invPerm(perm)</code> are column and row
index vectors, respectively.
</p>
<p>Given an <code>n</code>-by-<code>n</code> row permutation matrix <code>P</code>
with <code>perm</code> slot <code>p</code> and a matrix <code>M</code> with
conformable dimensions, we have
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>P M</i> </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>P %*% M</code>        </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>M[p, ]</code></td>
</tr>
<tr>
 <td style="text-align: left;">
    <i>M P</i> </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>M %*% P</code>        </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>M[, i(p)]</code></td>
</tr>
<tr>
 <td style="text-align: left;">
    <i>P'M</i> </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>crossprod(P, M)</code>  </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>M[i(p), ]</code></td>
</tr>
<tr>
 <td style="text-align: left;">
    <i>MP'</i> </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>tcrossprod(M, P)</code> </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>M[, p]</code></td>
</tr>
<tr>
 <td style="text-align: left;">
    <i>P'P</i> </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>crossprod(P)</code>     </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>Diagonal(n)</code></td>
</tr>
<tr>
 <td style="text-align: left;">
    <i>PP'</i> </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>tcrossprod(P)</code>    </td><td style="text-align: center;"> = </td><td style="text-align: left;"> <code>Diagonal(n)</code>
  </td>
</tr>

</table>

<p>where <code>i := invPerm</code>.
</p>


<h3>Objects from the Class</h3>

<p>Objects can be created explicitly with calls of the form
<code>new("pMatrix", ...)</code>, but they are more commonly created
by coercing 1-based integer index vectors, with calls of the
form <code>as(., "pMatrix")</code>; see &lsquo;Methods&rsquo; below.
</p>


<h3>Slots</h3>


<dl>
<dt><code>margin</code>,<code>perm</code></dt><dd><p>inherited from superclass
<code><a href="../../Matrix/help/indMatrix-class.html">indMatrix</a></code>.  Here, <code>perm</code> is an
integer vector of length <code>Dim[1]</code> and a permutation
of <code>1:Dim[1]</code>.</p>
</dd>
<dt><code>Dim</code>,<code>Dimnames</code></dt><dd><p>inherited from virtual
superclass <code><a href="../../Matrix/help/Matrix-class.html">Matrix</a></code>.</p>
</dd>
</dl>



<h3>Extends</h3>

<p>Class <code>"<a href="../../Matrix/help/indMatrix-class.html">indMatrix</a>"</code>, directly.
</p>


<h3>Methods</h3>


<dl>
<dt><code>%*%</code></dt><dd><p><code>signature(x = "pMatrix", y = "Matrix")</code>
and others listed by <code>showMethods("%*%", classes = "pMatrix")</code>:
matrix products implemented where appropriate as indexing operations.</p>
</dd>
<dt><code>coerce</code></dt><dd><p><code>signature(from = "numeric", to = "pMatrix")</code>:
supporting typical <code>pMatrix</code> construction from a vector
of positive integers, specifically a permutation of <code>1:n</code>.
Row permutation is assumed.</p>
</dd>
<dt>t</dt><dd><p><code>signature(x = "pMatrix")</code>:
the transpose, which is a <code>pMatrix</code> with identical
<code>perm</code> but opposite <code>margin</code>.  Coincides with
the inverse, as permutation matrices are orthogonal.</p>
</dd>
<dt>solve</dt><dd><p><code>signature(a = "pMatrix", b = "missing")</code>:
the inverse permutation matrix, which is a <code>pMatrix</code>
with identical <code>perm</code> but opposite <code>margin</code>.
Coincides with the transpose, as permutation matrices are
orthogonal.  See <code>showMethods("solve", classes = "pMatrix")</code>
for more signatures.</p>
</dd>
<dt>determinant</dt><dd><p><code>signature(x = "pMatrix", logarithm = "logical")</code>:
always returning 1 or -1, as permutation matrices are orthogonal.  
In fact, the result is exactly the <em>sign</em> of the permutation.</p>
</dd>
</dl>



<h3>See Also</h3>

<p>Superclass <code><a href="../../Matrix/help/indMatrix-class.html">indMatrix</a></code> of index matrices,
for many inherited methods; <code><a href="../../Matrix/help/invPerm.html">invPerm</a></code>, for computing
inverse permutation vectors.
</p>


<h3>Examples</h3>

<pre>

(pm1 &lt;- as(as.integer(c(2,3,1)), "pMatrix"))
t(pm1) # is the same as
solve(pm1)
pm1 %*% t(pm1) # check that the transpose is the inverse
stopifnot(all(diag(3) == as(pm1 %*% t(pm1), "matrix")),
          is.logical(as(pm1, "matrix")))

set.seed(11)
## random permutation matrix :
(p10 &lt;- as(sample(10),"pMatrix"))

## Permute rows / columns of a numeric matrix :
(mm &lt;- round(array(rnorm(3 * 3), c(3, 3)), 2))
mm %*% pm1
pm1 %*% mm
try(as(as.integer(c(3,3,1)), "pMatrix"))# Error: not a permutation

as(pm1, "TsparseMatrix")
p10[1:7, 1:4] # gives an "ngTMatrix" (most economic!)

## row-indexing of a &lt;pMatrix&gt; keeps it as an &lt;indMatrix&gt;:
p10[1:3, ]
</pre>

<hr /><div style="text-align: center;">[Package <em>Matrix</em> version 1.6-5 <a href="00Index.html">Index</a>]</div>
</body></html>
