<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Methods for LU Factorization</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for lu-methods {Matrix}"><tr><td>lu-methods {Matrix}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Methods for LU Factorization</h2>

<h3>Description</h3>

<p>Computes the pivoted LU factorization of an <i>m-by-n</i>
real matrix <i>A</i>, which has the general form
</p>
<p style="text-align: center;"><i>P1 * A * P2 = L * U</i></p>

<p>or (equivalently)
</p>
<p style="text-align: center;"><i>A = P1' * L * U * P2'</i></p>

<p>where
<i>P1</i> is an <i>m-by-m</i> permutation matrix, 
<i>P2</i> is an <i>n-by-n</i> permutation matrix, 
<i>L</i> is an <i>m-by-min(m,n)</i>
unit lower trapezoidal matrix, and
<i>U</i> is a  <i>min(m,n)-by-n</i>
upper trapezoidal matrix.
</p>
<p>Methods for <code><a href="../../Matrix/help/denseMatrix-class.html">denseMatrix</a></code> are built on
LAPACK routine <code>dgetrf</code>, which does not permute columns,
so that <i>P2</i> is an identity matrix.
</p>
<p>Methods for <code><a href="../../Matrix/help/sparseMatrix-class.html">sparseMatrix</a></code> are built on
CSparse routine <code>cs_lu</code>, which requires <i>m = n</i>,
so that <i>L</i> and <i>U</i> are triangular matrices.
</p>


<h3>Usage</h3>

<pre>
lu(x, ...)
## S4 method for signature 'dgeMatrix'
lu(x, warnSing = TRUE, ...)
## S4 method for signature 'dgCMatrix'
lu(x, errSing = TRUE, order = NA_integer_,
  tol = 1, ...)
## S4 method for signature 'dsyMatrix'
lu(x, cache = TRUE, ...)
## S4 method for signature 'dsCMatrix'
lu(x, cache = TRUE, ...)
## S4 method for signature 'matrix'
lu(x, ...)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>
<p>a <a href="../../Matrix/help/is.finite.html">finite</a> matrix or
<code><a href="../../Matrix/help/Matrix-class.html">Matrix</a></code> to be factorized,
which must be square if sparse.</p>
</td></tr>
<tr valign="top"><td><code>warnSing</code></td>
<td>
<p>a logical indicating if a <a href="../../base/html/warning.html">warning</a> should
be signaled for singular <code>x</code>.  Used only by methods for
dense matrices.</p>
</td></tr>
<tr valign="top"><td><code>errSing</code></td>
<td>
<p>a logical indicating if an <a href="../../base/html/stop.html">error</a> should
be signaled for singular <code>x</code>.  Used only by methods for
sparse matrices.</p>
</td></tr>
<tr valign="top"><td><code>order</code></td>
<td>
<p>an integer in <code>0:3</code> passed to CSparse routine
<code>cs_sqr</code>, indicating a strategy for choosing the column
permutation <i>P2</i>.  0 means no column permutation.
1, 2, and 3 indicate a fill-reducing ordering of <i>A + A'</i>,
<i>A~' * A~</i>, and <i>A' * A</i>,
where <i>A~</i> is <i>A</i> with &ldquo;dense&rdquo; rows
removed.
<code>NA</code> (the default) is equivalent to 2 if <code>tol == 1</code>
and 1 otherwise.
Do not set to 0 unless you know that the column order of <i>A</i>
is already sensible.</p>
</td></tr>
<tr valign="top"><td><code>tol</code></td>
<td>
<p>a number.  The original pivot element is used
if its absolute value exceeds <code>tol * a</code>,
where <code>a</code> is the maximum in absolute value of the
other possible pivot elements.
Set <code>tol &lt; 1</code> only if you know what you are doing.</p>
</td></tr>
<tr valign="top"><td><code>cache</code></td>
<td>
<p>a logical indicating if the result should be
cached in <code>x@factors[["LU"]]</code>.  Note that
caching is experimental and that only methods for classes
extending <code><a href="../../Matrix/help/compMatrix-class.html">compMatrix</a></code> will have this
argument.</p>
</td></tr>
<tr valign="top"><td><code>...</code></td>
<td>
<p>further arguments passed to or from methods.</p>
</td></tr>
</table>


<h3>Details</h3>

<p>What happens when <code>x</code> is determined to be near-singular
differs by method.  The method for class <code><a href="../../Matrix/help/dgeMatrix-class.html">dgeMatrix</a></code>
completes the factorization, warning if <code>warnSing = TRUE</code>
and in any case returning a valid <code><a href="../../Matrix/help/denseLU-class.html">denseLU</a></code>
object.  Users of this method can detect singular <code>x</code> with
a suitable warning handler; see <code><a href="../../base/html/conditions.html">tryCatch</a></code>.
In contrast, the method for class <code><a href="../../Matrix/help/dgCMatrix-class.html">dgCMatrix</a></code>
abandons further computation, throwing an error if <code>errSing = TRUE</code>
and otherwise returning <code>NA</code>.  Users of this method can
detect singular <code>x</code> with an error handler or by setting
<code>errSing = FALSE</code> and testing for a formal result with
<code>is(., "sparseLU")</code>.
</p>


<h3>Value</h3>

<p>An object representing the factorization, inheriting from
virtual class <code><a href="../../Matrix/help/LU-class.html">LU</a></code>.  The specific class
is <code><a href="../../Matrix/help/denseLU-class.html">denseLU</a></code> unless <code>x</code> inherits
from virtual class <code><a href="../../Matrix/help/sparseMatrix-class.html">sparseMatrix</a></code>,
in which case it is <code><a href="../../Matrix/help/sparseLU-class.html">sparseLU</a></code>.
</p>


<h3>References</h3>

<p>The LAPACK source code, including documentation; see
<a href="https://netlib.org/lapack/double/dgetrf.f">https://netlib.org/lapack/double/dgetrf.f</a>.
</p>
<p>Davis, T. A. (2006).
<em>Direct methods for sparse linear systems</em>.
Society for Industrial and Applied Mathematics.
doi: <a href="https://doi.org/10.1137/1.9780898718881">10.1137/1.9780898718881</a>
</p>
<p>Golub, G. H., &amp; Van Loan, C. F. (2013).
<em>Matrix computations</em> (4th ed.).
Johns Hopkins University Press.
doi: <a href="https://doi.org/10.56021/9781421407944">10.56021/9781421407944</a>
</p>


<h3>See Also</h3>

<p>Classes <code><a href="../../Matrix/help/denseLU-class.html">denseLU</a></code> and
<code><a href="../../Matrix/help/sparseLU-class.html">sparseLU</a></code> and their methods.
</p>
<p>Classes <code><a href="../../Matrix/help/dgeMatrix-class.html">dgeMatrix</a></code> and
<code><a href="../../Matrix/help/dgCMatrix-class.html">dgCMatrix</a></code>.
</p>
<p>Generic functions <code><a href="../../Matrix/help/expand1.html">expand1</a></code> and <code><a href="../../Matrix/help/expand2.html">expand2</a></code>,
for constructing matrix factors from the result.
</p>
<p>Generic functions <code><a href="../../Matrix/help/Cholesky.html">Cholesky</a></code>, <code><a href="../../Matrix/help/BunchKaufman.html">BunchKaufman</a></code>,
<code><a href="../../Matrix/help/Schur.html">Schur</a></code>, and <code><a href="../../Matrix/help/qr.html">qr</a></code>,
for computing other factorizations.
</p>


<h3>Examples</h3>

<pre>

showMethods("lu", inherited = FALSE)
set.seed(0)

## ---- Dense ----------------------------------------------------------

(A1 &lt;- Matrix(rnorm(9L), 3L, 3L))
(lu.A1 &lt;- lu(A1))

(A2 &lt;- round(10 * A1[, -3L]))
(lu.A2 &lt;- lu(A2))

## A ~ P1' L U in floating point
str(e.lu.A2 &lt;- expand2(lu.A2), max.level = 2L)
stopifnot(all.equal(A2, Reduce(`%*%`, e.lu.A2)))

## ---- Sparse ---------------------------------------------------------

A3 &lt;- as(readMM(system.file("external/pores_1.mtx", package = "Matrix")),
         "CsparseMatrix")
(lu.A3 &lt;- lu(A3))

## A ~ P1' L U P2' in floating point
str(e.lu.A3 &lt;- expand2(lu.A3), max.level = 2L)
stopifnot(all.equal(A3, Reduce(`%*%`, e.lu.A3)))
</pre>

<hr /><div style="text-align: center;">[Package <em>Matrix</em> version 1.6-5 <a href="00Index.html">Index</a>]</div>
</body></html>
