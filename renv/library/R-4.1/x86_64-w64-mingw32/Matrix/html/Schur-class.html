<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Schur Factorizations</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for Schur-class {Matrix}"><tr><td>Schur-class {Matrix}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Schur Factorizations</h2>

<h3>Description</h3>

<p><code>Schur</code> is the class of Schur factorizations of
<i>n-by-n</i> real matrices <i>A</i>,
having the general form
</p>
<p style="text-align: center;"><i>A = Q * T * Q'</i></p>

<p>where
<i>Q</i> is an orthogonal matrix and
<i>T</i> is a block upper triangular matrix with
<i>1-by-1</i> or <i>2-by-2</i> diagonal blocks
specifying the real and complex conjugate eigenvalues of <i>A</i>.
The column vectors of <i>Q</i> are the Schur vectors of <i>A</i>,
and <i>T</i> is the Schur form of <i>A</i>.
</p>
<p>The Schur factorization generalizes the spectral decomposition
of normal matrices <i>A</i>, whose Schur form is block diagonal,
to arbitrary square matrices.
</p>


<h3>Details</h3>

<p>The matrix <i>A</i> and its Schur form <i>T</i> are <em>similar</em>
and thus have the same spectrum.  The eigenvalues are computed
trivially as the eigenvalues of the diagonal blocks of <i>T</i>.
</p>


<h3>Slots</h3>


<dl>
<dt><code>Dim</code>, <code>Dimnames</code></dt><dd><p>inherited from virtual class
<code><a href="../../Matrix/help/MatrixFactorization-class.html">MatrixFactorization</a></code>.</p>
</dd>
<dt><code>Q</code></dt><dd><p>an orthogonal matrix,
inheriting from virtual class <code><a href="../../Matrix/help/Matrix-class.html">Matrix</a></code>.</p>
</dd>
<dt><code>T</code></dt><dd><p>a block upper triangular matrix,
inheriting from virtual class <code><a href="../../Matrix/help/Matrix-class.html">Matrix</a></code>.
The diagonal blocks have dimensions 1-by-1 or 2-by-2.</p>
</dd>
<dt><code>EValues</code></dt><dd><p>a numeric or complex vector containing
the eigenvalues of the diagonal blocks of <code>T</code>, which are
the eigenvalues of <code>T</code> and consequently of the factorized
matrix.</p>
</dd>
</dl>



<h3>Extends</h3>

<p>Class <code><a href="../../Matrix/help/SchurFactorization-class.html">SchurFactorization</a></code>, directly.
Class <code><a href="../../Matrix/help/MatrixFactorization-class.html">MatrixFactorization</a></code>, by class
<code><a href="../../Matrix/help/SchurFactorization-class.html">SchurFactorization</a></code>, distance 2.
</p>


<h3>Instantiation</h3>

<p>Objects can be generated directly by calls of the form
<code>new("Schur", ...)</code>, but they are more typically obtained
as the value of <code><a href="../../Matrix/help/Schur.html">Schur</a>(x)</code> for <code>x</code> inheriting from
<code><a href="../../Matrix/help/Matrix-class.html">Matrix</a></code> (often <code><a href="../../Matrix/help/dgeMatrix-class.html">dgeMatrix</a></code>).
</p>


<h3>Methods</h3>


<dl>
<dt><code>determinant</code></dt><dd><p><code>signature(from = "Schur", logarithm = "logical")</code>:
computes the determinant of the factorized matrix <i>A</i>
or its logarithm.</p>
</dd>
<dt><code>expand1</code></dt><dd><p><code>signature(x = "Schur")</code>:
see <code><a href="../../Matrix/help/expand1-methods.html">expand1-methods</a></code>.</p>
</dd>
<dt><code>expand2</code></dt><dd><p><code>signature(x = "Schur")</code>:
see <code><a href="../../Matrix/help/expand2-methods.html">expand2-methods</a></code>.</p>
</dd>
<dt><code>solve</code></dt><dd><p><code>signature(a = "Schur", b = .)</code>:
see <code><a href="../../Matrix/help/solve-methods.html">solve-methods</a></code>.</p>
</dd>
</dl>



<h3>References</h3>

<p>The LAPACK source code, including documentation; see
<a href="https://netlib.org/lapack/double/dgees.f">https://netlib.org/lapack/double/dgees.f</a>.
</p>
<p>Golub, G. H., &amp; Van Loan, C. F. (2013).
<em>Matrix computations</em> (4th ed.).
Johns Hopkins University Press.
doi: <a href="https://doi.org/10.56021/9781421407944">10.56021/9781421407944</a>
</p>


<h3>See Also</h3>

<p>Class <code><a href="../../Matrix/help/dgeMatrix-class.html">dgeMatrix</a></code>.
</p>
<p>Generic functions <code><a href="../../Matrix/help/Schur.html">Schur</a></code>,
<code><a href="../../Matrix/help/expand1.html">expand1</a></code> and <code><a href="../../Matrix/help/expand2.html">expand2</a></code>.
</p>


<h3>Examples</h3>

<pre>

showClass("Schur")
set.seed(0)

n &lt;- 4L
(A &lt;- Matrix(rnorm(n * n), n, n))

## With dimnames, to see that they are propagated :
dimnames(A) &lt;- list(paste0("r", seq_len(n)),
                    paste0("c", seq_len(n)))

(sch.A &lt;- Schur(A))
str(e.sch.A &lt;- expand2(sch.A), max.level = 2L)

## A ~ Q T Q' in floating point
stopifnot(exprs = {
    identical(names(e.sch.A), c("Q", "T", "Q."))
    all.equal(A, with(e.sch.A, Q %*% T %*% Q.))
})

## Factorization handled as factorized matrix
b &lt;- rnorm(n)
stopifnot(all.equal(det(A), det(sch.A)),
          all.equal(solve(A, b), solve(sch.A, b)))

## One of the non-general cases:
Schur(Diagonal(6L))
</pre>

<hr /><div style="text-align: center;">[Package <em>Matrix</em> version 1.6-5 <a href="00Index.html">Index</a>]</div>
</body></html>
