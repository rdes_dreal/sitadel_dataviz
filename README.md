# sitadel_dataviz

Code source de l'application de dataviz de sitadel publiée sur https://ssm-ecologie.shinyapps.io/sitadel

- `data-raw/dataprep.r` : script de préparation des données à lancer à chaque mise à jour des données.
- `data-raw/graph.r` : script de valorisation des données : créé 2 graphiques pour le mail d'alerte à lancer à chaque mise à jour des données après le script précédent.
- `app/` : scripts de l'application shiny à remettre en ligne à chaque mise à jour des données à lancer après `data-raw/dataprep.r`.
