# La base de données Sitadel2 : méthodologie

Source : "https://www.statistiques.developpement-durable.gouv.fr/la-base-de-donnees-sitadel2-methodologie?rubrique=&dossier=1047

Les données de Sitadel2 proviennent des formulaires de permis de construire. Tout pétitionnaire projetant une construction neuve ou la transformation d’une construction nécessitant le dépôt d’un permis de construire, doit remplir un formulaire relatif à son projet et le transmettre à la mairie de la commune de localisation des travaux. Le projet de permis est traité par les services instructeurs (État, collectivité territoriale) dont relève la commune. Suite à son autorisation, le pétitionnaire pourra soit démarrer les travaux et déclarer l’ouverture de son chantier (DOC) soit abandonner son projet (et demander l’annulation). La fin des travaux est signalée par une déclaration spécifique (DAACT), à partir de laquelle est vérifiée la conformité au projet initial.

## Source

### La source

Les données de Sitadel2 proviennent des formulaires de permis de construire traités par les centres instructeurs. Les mouvements relatifs à la vie du permis (dépôts, autorisations, annulations, modificatifs, mises en chantier, achèvements des travaux) sont exploités à des fins statistiques.    
Les informations déclarées dans le formulaire sont transmises mensuellement au service de la donnée et des études statistiques (SDES). Les informations relatives aux autorisations sont transmises par les services instructeurs dans les six mois après le prononcé. Les déclarations de mises en chantier et d’achèvement des travaux sont à l’initiative des pétitionnaires ; leur remontée est plus tardive et intervient généralement dans les dix-huit mois après l’ouverture de chantier.    
Les données collectées sont publiques.

 

### Les formulaires

Les données relatives aux autorisations de construire présentes dans la base Sitadel2 sont issues des formulaires suivants :
- le permis de construire (PC),
- le permis d’aménager (PA),
- la déclaration préalable (DP).
**Les permis pris en compte dans les statistiques de Sitadel2 sont ceux donnant lieu à des créations de logements ou à de la surface de locaux non résidentiels.**
Les données issues des permis de démolir sont enregistrées dans Sitadel2, mais elles ne sont pas statistiquement exploitables et ne font l’objet d’aucune diffusion.
Les informations relatives aux mises en chantier et aux achèvements de travaux sont issues des formulaires de déclaration d’ouverture de chantier (DOC) et de déclaration attestant l’achèvement et la conformité des travaux (DAACT). La transmission de ces documents est à l’initiative des pétitionnaires.

    Accéder aux modèles Cerfa 13406*03 et Cerfa 13409*03 téléchargeables sur le site http://www.service-public.fr/

 

## Les réformes récentes

La destination « local artisanal » a été introduite dans la nomenclature des destinations de construction à l’occasion de la réforme du droit du sol intervenue en octobre 2007. Auparavant, dans Sitadel, les superficies des locaux artisanaux étaient incluses dans les données relatives aux bâtiments industriels ou aux locaux de commerce.
Par ailleurs, les superficies des parkings et des aires de stationnement ne sont plus distinguées ni comptabilisées en tant que telles par Sitadel2.

[De Sitadel à Sit@del2 La](https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2018-11/De%20Sitadel%20%C3%A0%20Sitadel2_chart%C3%A9e%2022-08-2011.pdf)

À compter du 1er mars 2012, la surface de plancher remplace les précédentes SHON et SHOB. Cette évolution réglementaire induit une rupture de séries dans les données de surfaces collectées.

[S'informer sur la réforme de mars 2012](https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2018-11/en-savoirplus_0.pdf)

## Les données disponibles dans Sitadel2 : guide d'utilisation

### Les séries utilisées pour le diagnostic conjoncturel

La conjoncture est appréciée au travers d’**estimations en date réelle** dans le domaine du logement ; les locaux non résidentiels sont suivis grâce aux données en **date de prise en compte**.

Les séries estimées en date réelle visent à retracer dès le mois suivant les autorisations et les mises en chantier à la date réelle de l’événement. Majoritairement constituées de données collectées, les séries intègrent une estimation de l’information non remontée. Ces séries sont actualisées chaque mois pour prendre en compte les nouvelles informations. Pour s'informer sur la méthodologie :

[Télécharger la synthèse](https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2018-12/methodologie-sitadel2-v5.pdf)

[Télécharger le détail de la méthode](https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2019-02/doc-travail-21-methodologie-estimations-date-reelle-sitadel2-c.pdf)

Les estimations en date réelle offrent une information directement interprétable pour suivre la conjoncture ; elles corrigent notamment les éventuels défauts de collecte des déclarations d’ouverture de chantier. S’agissant d’estimations statistiques, leur qualité dépend du volume d’informations exploitées : en conséquence, ces séries ne sont disponibles qu’à des niveaux suffisamment agrégés (départemental au minimum).

 

### Les données issues de la collecte administrative

Ces séries sont disponibles au niveau communal.

Les **séries en date de prise** en compte agrègent l’information selon la date d’enregistrement de la donnée dans l’application Sitadel2. Par exemple : un permis autorisé en juin 2009 mais transmis en septembre 2009 est comptabilisé dans les autorisations de septembre 2009. Une mise en chantier transmise en octobre 2009 est enregistrée dans les mises en chantier d’octobre 2009 quand bien même la construction aurait démarrée en novembre 2008.

Les **séries en date réelle** agrègent l’information collectée (autorisations, mises en chantier) à la date réelle de l’autorisation délivrée par l’autorité compétente, et à la date de la mise en chantier déclarée par le pétitionnaire. Elles doivent être privilégiées aux séries en date de prise en compte pour les études locales, au niveau de la commune ou de l’EPCI.

Le délai de mise à disposition des séries en date réelle est relativement long (6 mois pour les autorisations, 18 mois pour les mises en chantier) compte tenu des délais de remontée de l’information. De plus, ces séries sont régulièrement révisées pour prendre en compte les informations les plus récentes connues pour chaque permis.
Par exemple : un permis autorisé en mars 2009 mais transmis en septembre 2009 ne pourra être intégré dans la série en date réelle de mars 2009 qu’au mois de septembre 2009.

*Note : les séries en date réelle des autorisations et des mises en chantier sont nettes des annulations.*

### Principales variables

Parmi l’ensemble des données collectées, les principales sont :
- le nombre et la surface de logements créés ;
- la surface créée des locaux non résidentiels.

Plusieurs types de construction sont distingués :
- quatre types de logements : logement individuel pur, logement individuel groupé, logement collectif, logement en résidence ;
- huit types de locaux non résidentiels : hébergement hôtelier, commerces, bureaux, artisanat, bâtiments industriels, entrepôts, exploitation agricole et forestière, services publics ou d’intérêt collectif. La catégorie « service public ou d'intérêt collectif » regroupe les six sous-catégories suivantes : transports, enseignement et recherche, action sociale, ouvrage spécial, santé, culture et loisirs.

*Attention : les foyers ou les hôtels qui ne comportent que des chambres et des services communs sont classés dans les locaux d'hébergement hôtelier et non dans les logements.
Les projets peuvent être mixtes, incluant à la fois la construction de logements et de locaux non résidentiels.*

## Service producteur et diffusion

Service de la donnée et des études statistiques (SDES)
Sous-direction des statistiques du logement et de la construction

### Diffusion

Les données de la construction (annuelles et mensuelles) issues de Sitadel2 sont accessibles :
- pour les [logements](https://www.statistiques.developpement-durable.gouv.fr/les-logements-neufs-0?rubrique=53) (voir la publication Construction de logements -résultats mensuels)
- pour les [locaux non résidentiels](https://www.statistiques.developpement-durable.gouv.fr/la-construction-de-locaux-non-residentiels-0?rubrique=47) (voir la publication Construction de locaux -résultats mensuels)
Les informations relatives aux permis de construire sont diffusées à la fin de chaque mois dans les rubriques liste des permis de construire des [logements](https://www.statistiques.developpement-durable.gouv.fr/liste-des-permis-de-construire-des-logements?rubrique=&dossier=1047) et des [locaux](https://www.statistiques.developpement-durable.gouv.fr/liste-des-permis-de-construire-des-locaux?rubrique=47&dossier=1053).

