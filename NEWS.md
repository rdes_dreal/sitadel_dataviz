# Version de développement

## Modification ux

- Ajout shinydreal
- Ajout mentionlegale.md
- Ajout source.md
- Mise des bornes sur l'axe des y en relatif
- Remplacement de shinyeffect par shinycustomloader
- Ajout d'un sélecteur pour définir une échelle fixe ou non sur l'axe des ordonnées pour les séries par départements/régions

## Modification serveur

- Rajout de l'ensemble des graphiques de l'application
- Mise en fonctin des graphiques
